package org.agiletestingalliance.cpdof.reexam;

import org.agiletestingalliance.cpdof.reexam.model.CertificationType;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(
        name = "selectatacertificationservlet",
        urlPatterns = "/SelectATACertifications"
)
public class SelectATACertificationServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String certType = req.getParameter("Type");

        ATAService ataService = new ATAService();
        CertificationType ct = CertificationType.valueOf(certType);

        List ataCertList = ataService.getAvailableBrands(ct);

        req.setAttribute("brands", ataCertList);
        RequestDispatcher view = req.getRequestDispatcher("result.jsp");
        view.forward(req, resp);

    }
}
